package me.tobi.ardacraft.controlpoints;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.ControlPoint;
import me.tobi.ardacraft.api.classes.Rank;

import me.tobi.ardacraft.api.classes.Region;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdControlpoint implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(Rank.get(p).isHigherThen(Rank.SPIELER)) {
				if(args.length < 1) {
					p.sendMessage("§2Hilfe:");
					p.sendMessage("§e/cp §acreate §e<Name> <Stadtname> - Erstellt einen ControlPoint an der Position des Spielers");
					p.sendMessage("§e/cp §cremove §e<Name> - Löscht den ControlPoint <Name>");
					p.sendMessage("§e/cp §blist§e - Listet alle in der Datenbank eingetragenen ControlPoints auf");
					return true;
				}
				if(args[0].equalsIgnoreCase("create")) {
					if(args.length == 3) {
						if(ArdaCraftAPI.getACDatabase().getCityManager().get(args[2]) != null) {
							if(ArdaCraftAPI.getACDatabase().getRegionManager().get(args[1]) == null) {
								Region.create(ArdaCraftAPI.getACDatabase().getCityManager().get(args[2]), args[1], p.getLocation().add(0, 1, 0));
								//Region rg = new ControlPoint(p.getLocation().add(0, 1, 0), ArdaCraftAPI.getACDatabase().getCity(args[2]), 0, args[1]);
							}else {
								p.sendMessage("§cEs existiert bereits ein ControlPoint mit diesem Namen!");
							}
						}else {
							p.sendMessage("§cBitte gib eine gültige Stadt an!");
						}
					}else{
						p.sendMessage("§c/cp create <Name> <Stadtname>");
					}
				}else if(args[0].equalsIgnoreCase("remove")) {
					if(args.length == 2) {
						Region rg = ArdaCraftAPI.getACDatabase().getRegionManager().get(args[1]);
						if(rg != null) {
							ArdaCraftAPI.getACDatabase().getRegionManager().delete(rg.getId());
							ControlPointManager.getInstance().removeControlPoint(rg);
							p.sendMessage("§aControlPoint " + args[1] + " wurde entfernt!");
						}else {
							p.sendMessage("§cDieser ControlPoint existiert nicht!");
						}
					}else {
						p.sendMessage("§c/cp remove <Name>");
					}
				}else if(args[0].equalsIgnoreCase("list")) {
					p.sendMessage("§eEingetragene ControlPoints:");
					p.sendMessage("=========================");
					for(Region rg : ArdaCraftAPI.getACDatabase().getRegionManager().getAll()) {
						if(rg.getCpState() > 0) {
							int prozent = (int)rg.getCpState() * 100 / 16;
							p.sendMessage("§e" + rg.getName() + ", §a" + prozent + "% gut");
						}else if(rg.getCpState() < 0) {
							int prozent = ((int)-1*rg.getCpState()) * 100 / 16;
							p.sendMessage("§e" + rg.getName() + ", §7" + prozent + "% böse");
						}else{
							p.sendMessage("§e" + rg.getName() + ", §f neutral");
						}
					}
					p.sendMessage("=========================");
				}else if(args[0].equalsIgnoreCase("info")) {
					if(args.length == 2) {
						Region rg = ArdaCraftAPI.getACDatabase().getRegionManager().get(args[1]);
						if(rg != null) {
							p.sendMessage("§aName: §e" + rg.getName());
							p.sendMessage("§aStadt: §e" + rg.getCity().getName());
							p.sendMessage("§aPosition: §e" + rg.getLocation().getBlockX() + ", " + rg.getLocation().getBlockY() + ", " + rg.getLocation().getBlockZ());
							p.sendMessage("§aStatus: " + getPercantage(rg.getCpState()));
							//p.sendMessage("§aWächter am Leben: " + 	GuardManager.getGuards(rg).size() + "/" + (GuardManager.getGuards(rg).size() + GuardManager.getDeadGuards(rg)));
						}else {
							p.sendMessage("§cBitte gib einen gültigen ControlPoint an!");
						}
					}else {
						p.sendMessage("§cBenutzung: /cp info <ControlPoint>");
					}
				}else if(args[0].equalsIgnoreCase("tp")) {
					if(args.length == 2) {
						Region rg = ArdaCraftAPI.getACDatabase().getRegionManager().get(args[1]);
						if (rg != null) {
							p.teleport(rg.getLocation().add(1, 2, 0));
							p.sendMessage("§aDu wurdest erfolgreich zum ControlPoint §e" + rg.getName() + " §ateleportiert!");
						}else {
							p.sendMessage("§cBitte gib einen gültigen ControlPoint an!");
						}
					}else {
						p.sendMessage("§cBenutzung: /cp tp <ControlPoint>");
					}
				}else {
					p.sendMessage("§2Hilfe:");
					p.sendMessage("§e/cp §acreate §e<Name> <Stadtname> - Erstellt einen ControlPoint an der Position des Spielers");
					p.sendMessage("§e/cp §cremove §e<Name> - Löscht den ControlPoint <Name>");
					p.sendMessage("§e/cp §blist§e - Listet alle in der Datenbank eingetragenen ControlPoints auf");
					p.sendMessage("§e/cp §binfo §e<Name> - Zeigt alle verfügbaren Informationen über den CP an");
					p.sendMessage("§e/cp §btp §e<Name> - Teleportiert dich zum entsprechenden CP");
					
				}
			}
		}
		return true;
	}

	public String getPercantage(int state) {
		if(state > 0) {
			int prozent = (int)state * 100 / 16;
			return "§a" + prozent + "% gut";
		}else if(state < 0) {
			int prozent = ((int)-1*state) * 100 / 16;
			return "§7" + prozent + "% böse";
		}else{
			return "§fNeutral";
		}
	}

}
