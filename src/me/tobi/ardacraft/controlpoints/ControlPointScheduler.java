package me.tobi.ardacraft.controlpoints;

import java.util.ArrayList;
import java.util.List;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.ControlPoint;
import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Region;
import me.tobi.ardacraft.api.classes.Utils.Attitude;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ControlPointScheduler {
	
	@SuppressWarnings("deprecation")
	public void run() {
		System.err.println("ControlPoint Scheduler started");
		Bukkit.getServer().getScheduler().scheduleAsyncRepeatingTask(ArdaCraftControlPoints.getPlugin(), 
				new Updater(), 20, 20);
	}
	
	public static long runs = 0;
	public static List<Region> rgs = null;
	
	class Updater implements Runnable {

		@Override
		public void run() {

            if(runs % 20 == 0 || rgs == null) {
                rgs = ArdaCraftAPI.getACDatabase().getRegionManager().getAll();
            }

            if(runs % 5*20 == 0){
                GuardManager.guardStroll(rgs);
            }

            for(final Region rg : rgs) {

                GuardManager.applyEffects(rg);

                Bukkit.getServer().getScheduler().runTask(ArdaCraftControlPoints.getPlugin(), new Runnable() {

                    @Override
                    public void run() {
                        updateCP(rg);
                        runs++;
                    }

                });
            }
		}
		
	}

	public void updateCP(Region rg){

	    List<Player> players = getPlayersNearControlPoint(rg);

        if(players.size() > 0){
            for(Player p : players){

                GuardManager.onPlayerNearControlPoint(rg, p);

                if(Rasse.get(p).getAttitude() == Attitude.GOOD) {
                    if(rg.getCpState() < 16) {
                        if(rg.getCpState() == 15) {
                            Bukkit.getServer().broadcastMessage("§eDer ControlPoint §a" + rg.getName() + " §ewurde von den Guten eingenommen!");
                            GuardManager.clearAttacks(rg);
                        }
                        if(GuardManager.allGuardsAreDead(rg)){
                            rg.setCpState(rg.getCpState() + 1);
                        }
                    }
                }else if(Rasse.get(p).getAttitude() == Attitude.BAD) {
                    if(rg.getCpState() > -16) {
                        if(rg.getCpState() == -15) {
                            Bukkit.getServer().broadcastMessage("§eDer ControlPoint §a" + rg.getName() + " §ewurde von den Bösen eingenommen!");
                            GuardManager.clearAttacks(rg);
                        }
                        if(GuardManager.allGuardsAreDead(rg)){
                            rg.setCpState(rg.getCpState() - 1);
                        }
                    }
                }else {
                    System.err.println("ControlPoints: Player is neither bad nor good");
                }
                ControlPointManager.getInstance().updateState(rg);
            }
        }else{
            GuardManager.onNoPlayersNearControlPoint(rg);
        }



    }

	public List<Player> getPlayersNearControlPoint(Region rg) {

	    List<Player> players = new ArrayList<>();

        for(Player p : Bukkit.getServer().getOnlinePlayers()){
            if(!p.getWorld().equals(rg.getLocation().getWorld())) {
                continue;
            }
            if(rg.getLocation().distance(p.getLocation()) < 15) {
                players.add(p);
            }
        }

        return players;
	}


}
