package me.tobi.ardacraft.controlpoints;

import java.util.ArrayList;
import java.util.List;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.*;
import me.tobi.ardacraft.api.classes.Utils.Attitude;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ControlPointListener implements Listener{
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(event.getClickedBlock().getType() == Material.BEACON) {
				Region rg = Utils.getNearestRegion(event.getClickedBlock().getLocation());
				if(rg == null) {
					System.out.println("No controlpoint in this world ControlPointListener.onPlayerInteract()");
					return;
				}
				if(rg.getLocation().distance(event.getClickedBlock().getLocation()) < 2) {
					event.setCancelled(true);
					Inventory inv = Bukkit.createInventory(p, 9*6, "Warp-Punkte");
					for(Region r : ArdaCraftAPI.getACDatabase().getRegionManager().getAll()) {
						ItemStack icon = new ItemStack(Material.BEACON);
						ItemMeta meta = icon.getItemMeta();
						meta.setDisplayName(r.getName());
						List<String> lore = new ArrayList<String>();
						lore.add("Stadt: " + r.getCity().getName());
						meta.setLore(lore);
						icon.setItemMeta(meta);
						if(Rasse.get(p).getAttitude() == Attitude.GOOD) {
							if(r.getCpState() == 16)
							inv.addItem(icon);
						}else if(Rasse.get(p).getAttitude() == Attitude.BAD) {
							if(r.getCpState() == -16)
							inv.addItem(icon);
						}
					}
					if(Rasse.get(p).getAttitude() == Attitude.GOOD && rg.getCpState() == 16) {
						p.openInventory(inv);
					}else if(Rasse.get(p).getAttitude() == Attitude.BAD && rg.getCpState() == -16) {
						p.openInventory(inv);
					}else {
						p.sendMessage("§cDu musst den ControlPoint erst komplett einnehmen bevor du die Warp-Funktion benutzen kannst!");
					}
				}
			}
		}
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
			Region rg = Utils.getNearestRegion(event.getClickedBlock().getLocation());
			if(rg == null) {
				return;
			}
			if(rg.getLocation().distance(event.getClickedBlock().getLocation()) < 5 && !p.isOp()) {
				event.setCancelled(true);
			}else if(rg.getLocation().distance(event.getClickedBlock().getLocation()) < 5 && p.isOp()){
				p.sendMessage("§cAchtung: Du baust gerade bei einem ControlPoint!");
			}
		}
			
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		try {
			if(event.getInventory().getName().equalsIgnoreCase("Warp-Punkte")) {
				event.setCancelled(true);
				Region rg = ArdaCraftAPI.getACDatabase().getRegionManager().get(event.getCurrentItem().getItemMeta().getDisplayName());
				((Player)event.getInventory().getHolder()).teleport(rg.getLocation().add(2, 2, 0));
				((Player)event.getInventory().getHolder()).sendMessage("§eDu wurdest zum ControlPoint " + event.getCurrentItem().getItemMeta().getDisplayName() + "§e teleportiert!");
				
			}
		}catch(NullPointerException ex) {
			
		}
	}
	
}
