package me.tobi.ardacraft.controlpoints.utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

public class RandomUtils {

    public static Location getRandomLocation(Location nearOf, int range, World world) {

        int x = randomInt(nearOf.getBlockX() - range, nearOf.getBlockX() + range);
        int y = 255;
        int z = randomInt(nearOf.getBlockZ() - range, nearOf.getBlockZ() + range);

        while (world.getBlockAt(x, y, z).getType() == Material.AIR) {
            y--;
        }

        return world.getBlockAt(x, y + 1, z).getLocation();
    }

    //min and max inclusive
    public static Integer randomInt(int min, int max){
        return (min + (int)(Math.random() * ((max - min) + 1)));
    }

}
